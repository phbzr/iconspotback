-- 05/03/20
ALTER SEQUENCE site_user_info_id_seq restart;
DO
$$
    DECLARE
        newId INTEGER := 1;
        maxi  INTEGER;

    begin
        SELECT MAX(id) into maxi FROM site_user;
        while newId < maxi
            loop
                select s.id into newId from site_user s where s.id > newId ORDER BY s.id;
                INSERT INTO site_user_info ("id", "owner_id")
                VALUES (nextval('site_user_info_id_seq'), newId);

            end loop;
    end;
$$
;

INSERT INTO "user_profile" ("id", "type")
VALUES (151, 'ADMIN');
INSERT INTO "user_profile" ("id", "type")
VALUES (152, 'USER');
INSERT INTO "user_profile" ("id", "type")
VALUES (153, 'CREATOR');
