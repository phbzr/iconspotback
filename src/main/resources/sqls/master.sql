-- 04/15/20
CREATE TABLE site_user
(
    id            SERIAL,
    sso_id        character varying(255) NOT NULL,
    password      character varying(255) NOT NULL,
    name          character varying(255),
    email         character varying(255),
    phone_number  character varying(255),
    avatar_path   text                   NOT NULL default
                                                      'https://www.spotteron.net/images/icons/user60.png',
    profile_photo text                   NOT NULL default
                                                      'https://sun9-29.userapi.com/wKH6k9-RA_-EKf4sTVH_zqsMLqcy2FPEnKhwuA/89sDzhm_X5M.jpg',
    CONSTRAINT site_user_pkey PRIMARY KEY (id),
    CONSTRAINT uk_mob09fcy788dek9kcq3aefj4k UNIQUE (sso_id)
)
    WITH (
        OIDS= FALSE
    );

CREATE TABLE user_profile
(
    id   SERIAL,
    type character varying(15) NOT NULL,
    CONSTRAINT user_profile_pkey PRIMARY KEY (id),
    CONSTRAINT uk_k6d5iiad3vb5isxj1munty17o UNIQUE (type)
)
    WITH (
        OIDS= FALSE
    );

CREATE TABLE app_user_profile
(
    user_id         integer NOT NULL,
    user_profile_id integer NOT NULL,
    CONSTRAINT app_user_user_profile_pkey PRIMARY KEY (user_id, user_profile_id),
    CONSTRAINT fk59naue7hu2wu76g38gmbbkdhu FOREIGN KEY (user_id)
        REFERENCES site_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT fksykc6x92tvtyxlaf9td3pt1st FOREIGN KEY (user_profile_id)
        REFERENCES user_profile (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
)
    WITH (
        OIDS= FALSE
    );


CREATE TABLE auth_token
(
    id            SERIAL NOT NULL,
    sso_id        text   NOT NULL,
    jwt_token     text   NOT NULL,
    refresh_token text   NOT NULL,
    CONSTRAINT auth_token_pkey PRIMARY KEY (id),
    CONSTRAINT uk_y5jso58a1f46g3n7ybbm5zbfr UNIQUE (jwt_token),
    CONSTRAINT uk_3jca35rk9njrrkh0rzs63jca3 UNIQUE (refresh_token)
)
    WITH (
        OIDS= FALSE
    );

CREATE TABLE post
(
    id            SERIAL  NOT NULL,
    owner_id      bigint  NOT NULL,
    title         text,
    content       text,
    likes_counter bigint  NOT NULL,
    views_counter bigint  NOT NULL,
    access        integer NOT NULL,
    cover_image   text,
    creation_date timestamp,

    CONSTRAINT post_pkey PRIMARY KEY (id),
    CONSTRAINT fk_post_site_user FOREIGN KEY (owner_id) REFERENCES site_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE
)
    WITH (
        OIDS= FALSE
    );

CREATE TABLE post_comment
(
    id             SERIAL NOT NULL,
    owner_id       bigint NOT NULL,
    post_id        bigint NOT NULL,
    parent_comment bigint,
    content        text,
    likes_counter  bigint NOT NULL,
    creation_date  timestamp,

    CONSTRAINT post_comment_pkey PRIMARY KEY (id),
    CONSTRAINT fk_post_comment_site_user FOREIGN KEY (owner_id) REFERENCES site_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT fk_post_comment_parent FOREIGN KEY (parent_comment) REFERENCES post_comment (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT fk_post_comment_post FOREIGN KEY (post_id) REFERENCES post (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE
)
    WITH (
        OIDS= FALSE
    );

CREATE TABLE post_attachment
(
    id          SERIAL NOT NULL,
    url         text   NOT NULL,
    type        bigint NOT NULL,
    owner_id    bigint NOT NULL,
    post_id     bigint NOT NULL,
    expiry_date timestamp,

    CONSTRAINT post_attachment_pkey PRIMARY KEY (id),
    CONSTRAINT fk_post_attachment_site_user FOREIGN KEY (owner_id) REFERENCES site_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT fk_post_attachment_post FOREIGN KEY (post_id) REFERENCES post (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE
)
    WITH (
        OIDS= FALSE
    );

-- 04/25/20

CREATE TABLE subscription_level_settings
(
    id                  SERIAL  NOT NULL,
    can_private_message boolean NOT NULL,

    CONSTRAINT subscription_level_settings_pkey PRIMARY KEY (id)
)
    WITH (
        OIDS= FALSE
    );


CREATE TABLE subscription_level
(
    id                          SERIAL                 NOT NULL,
    price                       bigint                 NOT NULL,
    title                       character varying(255) NOT NULL,
    description                 text,
    subscription_level_settings bigint                 NOT NULL,
    small_icon                  text,
    large_icon                  text,

    CONSTRAINT subscription_level_pkey PRIMARY KEY (id),
    CONSTRAINT fk_subscription_level_subscription_level_settings FOREIGN KEY
        (subscription_level_settings) REFERENCES subscription_level_settings (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE
)
    WITH (
        OIDS= FALSE
    );

CREATE TABLE user_relation_followers
(
    id        SERIAL NOT NULL,
    target_by bigint NOT NULL,
    target    bigint NOT NULL,

    CONSTRAINT user_relations_followers_pkey PRIMARY KEY (id),
    CONSTRAINT fk_target_by_site_user FOREIGN KEY
        (target_by) REFERENCES site_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT fk_target_site_user FOREIGN KEY
        (target) REFERENCES site_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE
)
    WITH (
        OIDS= FALSE
    );

CREATE TABLE user_relation_subscribers
(
    id                 SERIAL NOT NULL,
    target_by          bigint NOT NULL,
    target             bigint NOT NULL,
    subscription_level bigint NOT NULL,
    expiry_date        timestamp,

    CONSTRAINT user_relations_subscribers_pkey PRIMARY KEY (id),
    CONSTRAINT fk_target_by_site_user FOREIGN KEY
        (target_by) REFERENCES site_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT fk_target_site_user FOREIGN KEY
        (target) REFERENCES site_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE,
    CONSTRAINT fk_subscription_level_site_user FOREIGN KEY
        (subscription_level) REFERENCES subscription_level (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE
)
    WITH (
        OIDS= FALSE
    );

-- 04/27/20
CREATE TABLE post_subscription_access
(
    post_id               integer NOT NULL,
    subscription_level_id integer NOT NULL,
    CONSTRAINT post_subscription_access_pkey PRIMARY KEY (post_id, subscription_level_id),
    CONSTRAINT gdgasdfagretgdf32dsfsdfsfs2 FOREIGN KEY (post_id)
        REFERENCES post (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT dsad32g234gsgdsfa35iokjf431 FOREIGN KEY (subscription_level_id)
        REFERENCES subscription_level (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
)
    WITH (
        OIDS= FALSE
    );

-- 05/02/20

CREATE TABLE tag
(
    id  SERIAL NOT NULL,
    tag character varying(255),

    CONSTRAINT tag_pkey PRIMARY KEY (id)
)
    WITH (
        OIDS= FALSE
    );

CREATE TABLE post_tags
(
    post_id bigint NOT NULL,
    tag_id  bigint NOT NULL,
    CONSTRAINT post_tags_pkey PRIMARY KEY (post_id, tag_id),
    CONSTRAINT fdsfdsfc423fdsfa32fdsf FOREIGN KEY (post_id)
        REFERENCES post (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT fdsfdsfc423sffsdfdsa3g FOREIGN KEY (tag_id)
        REFERENCES tag (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE NO ACTION
)
    WITH (
        OIDS= FALSE
    );

-- 05/03/20

CREATE TABLE site_user_info
(
    id                SERIAL,
    owner_id          bigint NOT NULL,
    followers_count   bigint NOT NULL default 0,
    following_count   bigint NOT NULL default 0,
    subscribers_count bigint NOT NULL default 0,
    subscribing_count bigint NOT NULL default 0,
    views_count       bigint NOT NULL default 0,
    likes_count       bigint NOT NULL default 0,

    CONSTRAINT site_user_info_pkey PRIMARY KEY (id),
    CONSTRAINT fk_site_user_info_site_user FOREIGN KEY (owner_id) REFERENCES site_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION ON DELETE CASCADE
)
    WITH (
        OIDS= FALSE
    );
