package com.iconspotback.service;

import com.iconspotback.model.UserProfile;

public interface UserProfileService {

    UserProfile findByType(String type);
}
