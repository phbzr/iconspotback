package com.iconspotback.service;

import com.iconspotback.model.SiteUser;
import com.iconspotback.model.dto.SiteUserDto;
import com.iconspotback.model.dto.SiteUserDtoWithInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SiteUserService {

    SiteUser findBySsoId(String ssoId);

    SiteUser findByEmail(String email);

    boolean isUserSSOUnique(Long id, String sso);

    void saveUser(SiteUser siteUser);

    void editUser(SiteUser siteUser);

    SiteUser registerNewSocialUser(SiteUser socialUser);

    Page<SiteUserDto> searchUsers(String value, Pageable pageable);


    Page<SiteUserDto> recommendedUsers();

    SiteUserDtoWithInfo getUserWithInfoBySsoId(String ssoId);
}
