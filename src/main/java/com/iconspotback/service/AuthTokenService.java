package com.iconspotback.service;

import com.iconspotback.model.AuthToken;

public interface AuthTokenService {

    boolean validateToken(AuthToken authToken);

    void deleteBySSo(String ssoId);

    void delete(String jwtToken);

    void save(AuthToken authToken);

    AuthToken creatAuthTokens(String ssoId);
}
