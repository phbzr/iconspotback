package com.iconspotback.service;

import com.iconspotback.model.Post;
import com.iconspotback.model.dto.PostDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PostService {

    Post findById(Long postId);

    PostDto findDtoById(Long postId, boolean needComments, boolean needAttachments);

    Page<PostDto> findUserPosts(String ssoId, Integer access, Pageable pageable);

    Page<PostDto> findUserFeed(Long userId, Pageable pageable);

    Page<PostDto> findGlobalFeed(Long userId, Pageable pageable);

    void addPost(PostDto post);

    void deletePost(Long id);

    void editPost(PostDto post);
}
