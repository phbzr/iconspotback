package com.iconspotback.service.impl;

import com.iconspotback.service.SiteUserInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("siteUserInfoService")
@Transactional
public class SiteUserInfoServiceImpl implements SiteUserInfoService {
}
