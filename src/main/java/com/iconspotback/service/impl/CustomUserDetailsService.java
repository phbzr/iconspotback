package com.iconspotback.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.iconspotback.model.SiteUser;
import com.iconspotback.model.UserProfile;
import com.iconspotback.service.SiteUserService;

import java.util.ArrayList;
import java.util.List;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    public static final Logger LOGGER = LogManager.getLogger(CustomUserDetailsService.class);

    private SiteUserService userService;

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.debug("loadUserByUsername inside CustomUserDetailServiceImpl run with username: {}", username);
        SiteUser user = userService.findBySsoId(username);
        if (username.contains("@") && user == null) {
            user = userService.findByEmail(username);
        }
        if (user == null) {
            UsernameNotFoundException e = new UsernameNotFoundException("Username not found");
            LOGGER.error("loadUserByUsername failed with exception: ", e);
            throw e;
        }
        return new User(user.getSsoId(), user.getPassword(),
                true, true, true, true, getGrantedAuthorities(user));
    }


    private List<GrantedAuthority> getGrantedAuthorities(SiteUser user) {
        LOGGER.debug("getGrantedAuthorities inside CustomUserDetailServiceImpl run with user: {}", user);

        List<GrantedAuthority> authorities = new ArrayList<>();

        for (UserProfile userProfile : user.getUserProfiles()) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + userProfile.getType()));
        }
        LOGGER.debug("getGrantedAuthorities result: {}", authorities);
        return authorities;
    }

    @Autowired
    public void setUserService(SiteUserService userService) {
        this.userService = userService;
    }
}
