package com.iconspotback.service.impl;

import com.iconspotback.dao.PostAttachmentDao;
import com.iconspotback.dao.PostCommentDao;
import com.iconspotback.dao.PostDao;
import com.iconspotback.model.*;
import com.iconspotback.model.dto.*;
import com.iconspotback.service.DtoConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.*;

@Service("dtoConverter")
public class DtoConverterImpl implements DtoConverter {

    private PostCommentDao postCommentDao;
    private PostAttachmentDao postAttachmentDao;
    private PostDao postDao;
    private EntityManager entityManager;

    @Override
    public SiteUser convertSiteUserToModel(SiteUserDto siteUserDto) {
        SiteUser siteUser = new SiteUser();
        siteUser.setId(siteUserDto.getId());
        siteUser.setSsoId(siteUserDto.getSsoId());
        siteUser.setName(siteUserDto.getName());
        siteUser.setAvatarPath(siteUserDto.getAvatarPath());
        siteUser.setProfilePhoto(siteUserDto.getProfilePhoto());
        siteUser.setUserProfiles(siteUserDto.getUserProfiles());
        return siteUser;
    }

    @Override
    public SiteUserDto convertSiteUserToDto(SiteUser siteUser) {
        SiteUserDto siteUserDto = new SiteUserDto();
        siteUserDto.setId(siteUser.getId());
        siteUserDto.setSsoId(siteUser.getSsoId());
        siteUserDto.setName(siteUser.getName());
        siteUserDto.setAvatarPath(siteUser.getAvatarPath());
        siteUserDto.setProfilePhoto(siteUser.getProfilePhoto());
        siteUserDto.setUserProfiles(siteUser.getUserProfiles());
        return siteUserDto;
    }

    @Override
    public SiteUserDtoWithInfo convertSiteUserToDtoWithInfo(SiteUser siteUser, SiteUserInfo siteUserInfo) {
        SiteUserDto siteUserDto = convertSiteUserToDto(siteUser);
        SiteUserDtoWithInfo siteUserDtoWithInfo = new SiteUserDtoWithInfo(siteUserDto, siteUserInfo);
        return siteUserDtoWithInfo;
    }

    @Override
    public PostDto convertPostToDto(Post post, boolean needComments, boolean needAttachments) {
        PostDto postDto = new PostDto();
        postDto.setId(post.getId());
        postDto.setAccess(post.getAccess());
        postDto.setPostTags(post.getPostTags());
        postDto.setContent(post.getContent());
        postDto.setCoverImage(post.getCoverImage());
        postDto.setLikesCounter(post.getLikesCounter());
        postDto.setViewsCounter(post.getViewsCounter());
        postDto.setOwner(convertSiteUserToDto(post.getOwner()));
        postDto.setTitle(post.getTitle());
        postDto.setCreationDate(post.getCreationDate());
        if (post.getAccess() > 2) {
            Set<SubscriptionLevel> availableForSubscriptions = post.getAvailableForSubscriptions();
            Set<Long> subIds = new HashSet<>();
            for (SubscriptionLevel tmp : availableForSubscriptions) {
                subIds.add(tmp.getId());
            }
            postDto.setAvailableForSubscriptions(subIds);
        }
        if (needComments) {
            Page<PostComment> byPost = postCommentDao.findByPost(post.getId(), PageRequest.of(0, 20));
            List<PostComment> content = byPost.getContent();
            List<PostCommentDto> postCommentDtos = convertPostCommentsToDto(content);
            postDto.setComments(new PageImpl<>(postCommentDtos, byPost.getPageable(), byPost.getTotalElements()));
        }
        if (needAttachments) {
            List<PostAttachment> postAttachmentsByPostId = postAttachmentDao.getPostAttachmentsByPostId(post.getId());
            List<PostAttachmentDto> postAttachmentDtos = convertPostAttachmentsToDto(postAttachmentsByPostId);
            postDto.setAttachments(postAttachmentDtos);
        }
        return postDto;
    }

    @Override
    public List<PostDto> covertPostsToDto(List<Post> posts, boolean needComments, boolean needAttachments) {
        List<PostDto> result = new ArrayList<>();
        for (Post post : posts) {
            result.add(convertPostToDto(post, needComments, needAttachments));
        }
        return result;
    }

    @Override
    public List<PostCommentDto> convertPostCommentsToDto(List<PostComment> postComments) {
        List<PostCommentDto> result = new ArrayList<>();
        for (PostComment postComment : postComments) {
            result.add(convertPostCommentToDto(postComment));
        }
        return result;
    }

    @Override
    public PostCommentDto convertPostCommentToDto(PostComment postComment) {
        PostCommentDto postCommentDto = new PostCommentDto();
        postCommentDto.setId(postComment.getId());
        postCommentDto.setContent(postComment.getContent());
        postCommentDto.setLikesCounter(postComment.getLikesCounter());
        postCommentDto.setOwner(convertSiteUserToDto(postComment.getOwner()));
        postCommentDto.setPostId(postComment.getPost().getId());
        postCommentDto.setCreationDate(postComment.getCreationDate());

        PostComment parentComment = postComment.getParentComment();
        if (parentComment != null) {
            postCommentDto.setParentCommentId(parentComment.getId());
        }
        return postCommentDto;
    }

    @Override
    public PostAttachmentDto convertPostAttachmentToDto(PostAttachment postAttachment) {
        PostAttachmentDto postAttachmentDto = new PostAttachmentDto();
        postAttachmentDto.setId(postAttachment.getId());
        postAttachmentDto.setPostId(postAttachment.getPost().getId());
        postAttachmentDto.setType(postAttachment.getType());
        postAttachmentDto.setUrl(postAttachment.getUrl());
        return postAttachmentDto;
    }

    @Override
    public List<PostAttachmentDto> convertPostAttachmentsToDto(List<PostAttachment> postAttachments) {
        List<PostAttachmentDto> result = new ArrayList<>();
        for (PostAttachment postAttachment : postAttachments) {
            result.add(convertPostAttachmentToDto(postAttachment));
        }
        return result;
    }

    @Override
    public List<SiteUserDto> convertSiteUsersToDto(List<SiteUser> siteUsers) {
        List<SiteUserDto> result = new ArrayList<>();
        for (SiteUser siteUser : siteUsers) {
            SiteUserDto siteUserDto = convertSiteUserToDto(siteUser);
            result.add(siteUserDto);
        }
        return result;
    }

    @Override
    public Post convertPostToNewModel(PostDto postDto) {
        Post post = new Post();
        post.setAccess(postDto.getAccess());
        post.setPostTags(postDto.getPostTags());
        post.setContent(postDto.getContent());
        post.setCoverImage(postDto.getCoverImage());
        post.setLikesCounter(postDto.getLikesCounter());
        post.setTitle(postDto.getTitle());
        post.setViewsCounter(postDto.getViewsCounter());
        post.setCreationDate(postDto.getCreationDate());
//        post.setOwner(convertSiteUserToModel(postDto.getOwner()));
        if (post.getAccess() > 2) {
            post.setAvailableForSubscriptions(getSubLevelsFromIds(postDto.getAvailableForSubscriptions()));
        }
        return post;
    }

    @Override
    public Post convertPostToExistingModel(PostDto postDto) {
        Optional<Post> postOptional = postDao.findById(postDto.getId());
        Post post = postOptional.orElseGet(Post::new);
        post.setViewsCounter(postDto.getViewsCounter());
        post.setLikesCounter(postDto.getLikesCounter());
        post.setTitle(postDto.getTitle());
        post.setCoverImage(postDto.getCoverImage());
        post.setContent(postDto.getContent());
        post.setPostTags(postDto.getPostTags());
        post.setAccess(postDto.getAccess());
        if (post.getAccess() > 2) {
            post.setAvailableForSubscriptions(getSubLevelsFromIds(postDto.getAvailableForSubscriptions()));
        }
        return post;
    }

    private Set<SubscriptionLevel> getSubLevelsFromIds(Set<Long> subIds) {
        Set<SubscriptionLevel> subscriptionLevels = new HashSet<>();
        for (Long tmp : subIds) {
            SubscriptionLevel subscriptionLevel = entityManager.getReference(SubscriptionLevel.class, tmp);
            subscriptionLevels.add(subscriptionLevel);
        }
        return subscriptionLevels;
    }

    @Autowired
    public void setPostCommentDao(PostCommentDao postCommentDao) {
        this.postCommentDao = postCommentDao;
    }

    @Autowired
    public void setPostAttachmentDao(PostAttachmentDao postAttachmentDao) {
        this.postAttachmentDao = postAttachmentDao;
    }

    @Autowired
    public void setPostDao(PostDao postDao) {
        this.postDao = postDao;
    }

    @Autowired
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}
