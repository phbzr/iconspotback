package com.iconspotback.service.impl;

import com.iconspotback.dao.PostDao;
import com.iconspotback.model.Post;
import com.iconspotback.model.SiteUser;
import com.iconspotback.model.dto.PostDto;
import com.iconspotback.service.AuthService;
import com.iconspotback.service.DtoConverter;
import com.iconspotback.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service("postService")
@Transactional
public class PostServiceImpl implements PostService {

    private PostDao postDao;
    private DtoConverter dtoConverter;
    private AuthService authService;

    @Override
    public Post findById(Long postId) {
        Optional<Post> post = postDao.findById(postId);
        return post.orElseGet(Post::new);
    }

    @Override
    public PostDto findDtoById(Long postId, boolean needComments, boolean needAttachments) {
        Post post = findById(postId);
        if (post.getId() == null) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND);
        }
        PostDto result = dtoConverter.convertPostToDto(post, needComments, needAttachments);
        return result;
    }

    @Override
    public Page<PostDto> findUserPosts(String ssoId, Integer access, Pageable pageable) {
        Page<Post> postPage = postDao.findByUser(ssoId, pageable);
        List<Post> content = postPage.getContent();
        List<PostDto> postDtos = dtoConverter.covertPostsToDto(content, false, false);
        Page<PostDto> result = new PageImpl<>(postDtos, pageable, postPage.getTotalElements());
        return result;
    }

    @Override
    public Page<PostDto> findUserFeed(Long userId, Pageable pageable) {
        return null;
    }

    @Override
    public Page<PostDto> findGlobalFeed(Long userId, Pageable pageable) {
        return null;
    }

    @Override
    public void addPost(PostDto postDto) {
        Post post = dtoConverter.convertPostToNewModel(postDto);
        post.setCreationDate(new Date());
        SiteUser authUser = authService.getAuthUser();
        post.setOwner(authUser);
        post.setLikesCounter(0L);
        post.setViewsCounter(0L);
        postDao.save(post);
    }

    @Override
    public void deletePost(Long id) {
        postDao.deleteById(id);
    }

    @Override
    public void editPost(PostDto postDto) {
        Post post = dtoConverter.convertPostToExistingModel(postDto);
        postDao.save(post);
    }

    @Autowired
    public void setPostDao(PostDao postDao) {
        this.postDao = postDao;
    }

    @Autowired
    public void setDtoConverter(DtoConverter dtoConverter) {
        this.dtoConverter = dtoConverter;
    }

    @Autowired
    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }
}
