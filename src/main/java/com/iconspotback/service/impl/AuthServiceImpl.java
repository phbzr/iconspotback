package com.iconspotback.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import com.iconspotback.model.SiteUser;
import com.iconspotback.service.AuthService;
import com.iconspotback.service.SiteUserService;

@Service("authService")
public class AuthServiceImpl implements AuthService {

    private AuthenticationManager authenticationManager;
    private SiteUserService siteUserService;

    @Override
    public Authentication authenticate(String username, String password) throws Exception {
        try {
            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

    @Override
    public boolean hasRole(String role) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication.getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals(role));
    }

    @Override
    public SiteUser getAuthUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return siteUserService.findBySsoId(authentication.getName());
    }

    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    @Autowired
    public void setSiteUserService(SiteUserService siteUserService) {
        this.siteUserService = siteUserService;
    }
}
