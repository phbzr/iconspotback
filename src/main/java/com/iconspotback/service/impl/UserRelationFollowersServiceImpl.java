package com.iconspotback.service.impl;

import com.iconspotback.service.UserRelationFollowersService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userRelationFollowersService")
@Transactional
public class UserRelationFollowersServiceImpl implements UserRelationFollowersService {
}
