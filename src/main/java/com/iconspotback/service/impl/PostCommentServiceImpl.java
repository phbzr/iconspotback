package com.iconspotback.service.impl;

import com.iconspotback.service.PostCommentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("postCommentService")
@Transactional
public class PostCommentServiceImpl implements PostCommentService {
}
