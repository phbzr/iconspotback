package com.iconspotback.service.impl;

import com.iconspotback.service.PostAttachmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("postAttachmentService")
@Transactional
public class PostAttachmentServiceImpl implements PostAttachmentService {
}
