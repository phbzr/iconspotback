package com.iconspotback.service.impl;

import com.iconspotback.service.SubscriptionLevelService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("subscriptionLevelService")
@Transactional
public class SubscriptionLevelServiceImpl implements SubscriptionLevelService {
}
