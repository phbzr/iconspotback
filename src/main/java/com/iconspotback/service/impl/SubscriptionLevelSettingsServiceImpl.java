package com.iconspotback.service.impl;

import com.iconspotback.service.SubscriptionLevelSettingsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("subscriptionLevelSettingsService")
@Transactional
public class SubscriptionLevelSettingsServiceImpl implements SubscriptionLevelSettingsService {
}
