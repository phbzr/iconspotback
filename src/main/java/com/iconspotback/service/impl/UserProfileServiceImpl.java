package com.iconspotback.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.iconspotback.dao.UserProfileDao;
import com.iconspotback.model.UserProfile;
import com.iconspotback.service.UserProfileService;

@Service("userProfileService")
@Transactional
public class UserProfileServiceImpl implements UserProfileService {

    private UserProfileDao userProfileDao;

    @Override
    public UserProfile findByType(String type) {
        return userProfileDao.findByType(type);
    }

    @Autowired
    public void setUserProfileDao(UserProfileDao userProfileDao) {
        this.userProfileDao = userProfileDao;
    }
}
