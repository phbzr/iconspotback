package com.iconspotback.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.iconspotback.config.JwtService;
import com.iconspotback.dao.AuthTokenDao;
import com.iconspotback.model.AuthToken;
import com.iconspotback.service.AuthTokenService;

import java.util.UUID;

@Service("authTokenService")
@Transactional
public class AuthTokenServiceImpl implements AuthTokenService {

    private AuthTokenDao authTokenDao;
    private JwtService jwtService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    @Qualifier("customUserDetailsService")
    private UserDetailsService userDetailsService;

    @Override
    public boolean validateToken(AuthToken authToken) {
        AuthToken tokenPair = authTokenDao.getAuthTokenByJwtTokenAndRefreshToken(authToken.getJwtToken(), authToken.getRefreshToken());
        return tokenPair.getId() != null;
    }

    @Override
    public void deleteBySSo(String ssoId) {
        authTokenDao.deleteBySsoId(ssoId);
    }

    @Override
    public void delete(String jwtToken) {
        authTokenDao.deleteByJwtToken(jwtToken);
    }

    @Override
    public AuthToken creatAuthTokens(String ssoId) {
        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(ssoId);
        final String token = jwtService.generateToken(userDetails);
        final String refreshToken = passwordEncoder.encode(UUID.randomUUID().toString());

        AuthToken authToken = new AuthToken(token, refreshToken, ssoId, Long.parseLong(jwtService.JWT_TOKEN_VALIDITY) - 10);
        authTokenDao.save(authToken);
        return authToken;
    }

    @Override
    public void save(AuthToken authToken) {
        authTokenDao.save(authToken);
    }

    @Autowired
    public void setAuthTokenDao(AuthTokenDao authTokenDao) {
        this.authTokenDao = authTokenDao;
    }

    @Autowired
    public void setJwtService(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
}
