package com.iconspotback.service.impl;

import com.iconspotback.service.UserRelationSubscribersService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userRelationSubscribersService")
@Transactional
public class UserRelationSubscribersServiceImpl implements UserRelationSubscribersService {
}
