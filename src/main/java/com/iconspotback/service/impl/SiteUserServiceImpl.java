package com.iconspotback.service.impl;

import com.iconspotback.dao.SiteUserDao;
import com.iconspotback.dao.SiteUserInfoDao;
import com.iconspotback.model.SiteUser;
import com.iconspotback.model.SiteUserInfo;
import com.iconspotback.model.UserProfile;
import com.iconspotback.model.dto.SiteUserDto;
import com.iconspotback.model.dto.SiteUserDtoWithInfo;
import com.iconspotback.service.AuthService;
import com.iconspotback.service.DtoConverter;
import com.iconspotback.service.SiteUserService;
import com.iconspotback.service.UserProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

@Service("siteUserService")
@Transactional
public class SiteUserServiceImpl implements SiteUserService {

    private SiteUserDao siteUserDao;
    private PasswordEncoder passwordEncoder;
    private UserProfileService userProfileService;
    private AuthService authService;
    private DtoConverter dtoConverter;
    private SiteUserInfoDao siteUserInfoDao;

    @Override
    public SiteUser findBySsoId(String ssoId) {
        return siteUserDao.findBySsoId(ssoId);
    }

    @Override
    public SiteUserDtoWithInfo getUserWithInfoBySsoId(String ssoId) {
        SiteUser siteUser = siteUserDao.findBySsoId(ssoId);
        SiteUserInfo siteUserInfo = siteUserInfoDao.getByOwnerId(siteUser.getId());
        SiteUserDtoWithInfo siteUserDtoWithInfo = dtoConverter.convertSiteUserToDtoWithInfo(siteUser, siteUserInfo);
        return siteUserDtoWithInfo;
    }

    @Override
    public SiteUser findByEmail(String email) {
        return siteUserDao.findByEmail(email);
    }

    @Override
    public boolean isUserSSOUnique(Long id, String sso) {
        SiteUser user = findBySsoId(sso);
        return (user == null || ((id != null) && (user.getId() == id)));
    }

    @Override
    public void saveUser(SiteUser siteUser) {
        UserProfile userProfile = userProfileService.findByType("USER");
        Set<UserProfile> setOfProfiles = new HashSet();
        setOfProfiles.add(userProfile);
        siteUser.setUserProfiles(setOfProfiles);

        siteUser.setPassword(passwordEncoder.encode(siteUser.getPassword()));
        siteUserDao.save(siteUser);
        SiteUserInfo siteUserInfo = new SiteUserInfo();
        siteUserInfo.setOwner(siteUser);

        siteUserInfoDao.save(siteUserInfo);
    }

    @Override
    public void editUser(SiteUser siteUser) {
        siteUserDao.save(siteUser);
    }

    @Override
    public SiteUser registerNewSocialUser(SiteUser socialUser) {
        socialUser.setPassword(UUID.randomUUID().toString());

        UserProfile userProfile = userProfileService.findByType("USER");
        Set<UserProfile> setOfProfiles = new HashSet();
        setOfProfiles.add(userProfile);
        socialUser.setUserProfiles(setOfProfiles);
        saveUser(socialUser);
        return socialUser;
    }

    @Override
    public Page<SiteUserDto> searchUsers(String value, Pageable pageable) {
        SiteUser authUser = authService.getAuthUser();
        Page<SiteUser> siteUsers = siteUserDao.searchUsers(value, pageable, authUser.getId());
        List<SiteUser> content = siteUsers.getContent();
        List<SiteUserDto> contentDtos = dtoConverter.convertSiteUsersToDto(content);
        Page<SiteUserDto> result = new PageImpl<>(contentDtos, pageable, siteUsers.getTotalElements());
        return result;
    }

    @Override
    public Page<SiteUserDto> recommendedUsers() {
        return null;
    }

    @Autowired
    public void setSiteUserDao(SiteUserDao siteUserDao) {
        this.siteUserDao = siteUserDao;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserProfileService(UserProfileService userProfileService) {
        this.userProfileService = userProfileService;
    }

    @Autowired
    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }

    @Autowired
    public void setDtoConverter(DtoConverter dtoConverter) {
        this.dtoConverter = dtoConverter;
    }

    @Autowired
    public void setSiteUserInfoDao(SiteUserInfoDao siteUserInfoDao) {
        this.siteUserInfoDao = siteUserInfoDao;
    }
}
