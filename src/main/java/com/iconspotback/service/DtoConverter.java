package com.iconspotback.service;

import com.iconspotback.model.*;
import com.iconspotback.model.dto.*;

import java.util.List;

public interface DtoConverter {

    SiteUser convertSiteUserToModel(SiteUserDto siteUserDto);

    SiteUserDto convertSiteUserToDto(SiteUser siteUser);

    List<SiteUserDto> convertSiteUsersToDto(List<SiteUser> siteUsers);

    PostDto convertPostToDto(Post post, boolean needComments, boolean needAttachments);

    Post convertPostToNewModel(PostDto postDto);

    Post convertPostToExistingModel(PostDto postDto);

    List<PostDto> covertPostsToDto(List<Post> posts, boolean needComments, boolean needAttachments);

    PostCommentDto convertPostCommentToDto(PostComment postComment);

    List<PostCommentDto> convertPostCommentsToDto(List<PostComment> postComments);

    PostAttachmentDto convertPostAttachmentToDto(PostAttachment postAttachment);

    List<PostAttachmentDto> convertPostAttachmentsToDto(List<PostAttachment> postAttachments);

    SiteUserDtoWithInfo convertSiteUserToDtoWithInfo(SiteUser siteUser, SiteUserInfo siteUserInfo);
}
