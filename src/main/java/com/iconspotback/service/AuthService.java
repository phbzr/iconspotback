package com.iconspotback.service;

import org.springframework.security.core.Authentication;
import com.iconspotback.model.SiteUser;

public interface AuthService {
    Authentication authenticate(String username, String password) throws Exception;

    /**
     * @param role user role, available roles: ROLE_ADMIN, ROLE_USER
     * @return does authenticated user has this role or not
     */
    boolean hasRole(String role);

    SiteUser getAuthUser();
}
