package com.iconspotback.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan(basePackages = {"com.iconspotback.*"})
public class ApplicationContext implements WebMvcConfigurer {

}
