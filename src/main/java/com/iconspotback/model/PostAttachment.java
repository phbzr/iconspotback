package com.iconspotback.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "post_attachment")
public class PostAttachment extends Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "post_attachment_generator")
    @SequenceGenerator(allocationSize = 1, name = "post_attachment_generator", sequenceName = "post_attachment_sequence")
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Post post;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    @Override
    public String toString() {
        return "PostAttachment{" +
                "id=" + id +
                ", post=" + post +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostAttachment that = (PostAttachment) o;

        if (!id.equals(that.id)) return false;
        return post.equals(that.post);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + post.hashCode();
        return result;
    }
}
