package com.iconspotback.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "site_user")
public class SiteUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "site_user_generator")
    @SequenceGenerator(allocationSize = 1, name = "site_user_generator", sequenceName = "site_user_id_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "sso_id", unique = true, nullable = false)
    private String ssoId;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "avatar_path", nullable = false)
    private String avatarPath = "https://www.spotteron.net/images/icons/user60.png";

    @Column(name = "profile_photo", nullable = false)
    private String profilePhoto = "https://sun9-29.userapi.com/wKH6k9-RA_-EKf4sTVH_zqsMLqcy2FPEnKhwuA/89sDzhm_X5M.jpg";

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "app_user_profile",
            joinColumns = {@JoinColumn(name = "USER_ID")},
            inverseJoinColumns = {@JoinColumn(name = "USER_PROFILE_ID")})
    private Set<UserProfile> userProfiles = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSsoId() {
        return ssoId;
    }

    public void setSsoId(String ssoId) {
        this.ssoId = ssoId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public Set<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(Set<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SiteUser siteUser = (SiteUser) o;

        if (!id.equals(siteUser.id)) return false;
        if (!ssoId.equals(siteUser.ssoId)) return false;
        if (!password.equals(siteUser.password)) return false;
        if (name != null ? !name.equals(siteUser.name) : siteUser.name != null) return false;
        if (email != null ? !email.equals(siteUser.email) : siteUser.email != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(siteUser.phoneNumber) : siteUser.phoneNumber != null)
            return false;
        if (!avatarPath.equals(siteUser.avatarPath)) return false;
        if (!profilePhoto.equals(siteUser.profilePhoto)) return false;
        return userProfiles != null ? userProfiles.equals(siteUser.userProfiles) : siteUser.userProfiles == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + ssoId.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
        result = 31 * result + avatarPath.hashCode();
        result = 31 * result + profilePhoto.hashCode();
        result = 31 * result + (userProfiles != null ? userProfiles.hashCode() : 0);
        return result;
    }
}
