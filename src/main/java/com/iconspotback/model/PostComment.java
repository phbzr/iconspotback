package com.iconspotback.model;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "post_comment")
public class PostComment extends Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "post_comment_generator")
    @SequenceGenerator(allocationSize = 1, name = "post_comment_generator", sequenceName = "post_comment_id_seq")
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "post_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Post post;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parent_comment")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private PostComment parentComment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public PostComment getParentComment() {
        return parentComment;
    }

    public void setParentComment(PostComment parentComment) {
        this.parentComment = parentComment;
    }

    @Override
    public String toString() {
        return "PostComment{" +
                "id=" + id +
                ", post=" + post +
                ", parentComment=" + parentComment +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostComment that = (PostComment) o;

        if (!id.equals(that.id)) return false;
        if (!post.equals(that.post)) return false;
        return parentComment != null ? parentComment.equals(that.parentComment) : that.parentComment == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + post.hashCode();
        result = 31 * result + (parentComment != null ? parentComment.hashCode() : 0);
        return result;
    }
}
