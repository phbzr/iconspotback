package com.iconspotback.model;

import javax.persistence.*;

@Entity
@Table(name = "auth_token")
public class AuthToken {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auth_token_generator")
    @SequenceGenerator(allocationSize = 1, name = "auth_token_generator", sequenceName = "auth_token_id_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "sso_id", nullable = false)
    private String ssoId;

    @Column(name = "jwt_token", nullable = false, unique = true)
    private String jwtToken;

    @Column(name = "refresh_token", nullable = false, unique = true)
    private String refreshToken;

    @Transient
    private Long liveTimeSec;

    public AuthToken() {
    }

    public AuthToken(String jwtToken, String refreshToken, String ssoId, Long liveTimeSec) {
        this.jwtToken = jwtToken;
        this.refreshToken = refreshToken;
        this.ssoId = ssoId;
        this.liveTimeSec = liveTimeSec;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJwtToken() {
        return jwtToken;
    }

    public void setJwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getSsoId() {
        return ssoId;
    }

    public void setSsoId(String ssoId) {
        this.ssoId = ssoId;
    }

    public Long getLiveTimeSec() {
        return liveTimeSec;
    }

    public void setLiveTimeSec(Long liveTimeSec) {
        this.liveTimeSec = liveTimeSec;
    }

    @Override
    public String toString() {
        return "AuthToken{" +
                "id=" + id +
                ", ssoId='" + ssoId + '\'' +
                ", jwtToken='" + jwtToken + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthToken that = (AuthToken) o;

        if (!ssoId.equals(that.ssoId)) return false;
        if (!jwtToken.equals(that.jwtToken)) return false;
        return refreshToken.equals(that.refreshToken);
    }

    @Override
    public int hashCode() {
        int result = ssoId.hashCode();
        result = 31 * result + jwtToken.hashCode();
        result = 31 * result + refreshToken.hashCode();
        return result;
    }
}
