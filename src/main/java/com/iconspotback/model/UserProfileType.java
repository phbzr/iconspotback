package com.iconspotback.model;

import java.io.Serializable;

public enum UserProfileType implements Serializable {
    USER("USER"),
    CREATOR("CREATOR"),
    ADMIN("ADMIN");

    String userProfileType;

    private UserProfileType(String userProfileType) {
        this.userProfileType = userProfileType;
    }

    public String getUserProfileType() {
        return userProfileType;
    }

}
