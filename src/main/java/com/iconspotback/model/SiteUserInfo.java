package com.iconspotback.model;

import javax.persistence.*;

@Entity
@Table(name = "site_user_info")
public class SiteUserInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "site_user_info_generator")
    @SequenceGenerator(allocationSize = 1, name = "site_user_info_generator", sequenceName = "site_user_info_id_seq")
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", nullable = false)
    private SiteUser owner;

    @Column(name = "followers_count", nullable = false, columnDefinition = "default 0")
    private Long followersCount = 0L;

    @Column(name = "following_count", nullable = false, columnDefinition = "default 0")
    private Long followingCount = 0L;

    @Column(name = "subscribers_count", nullable = false, columnDefinition = "default 0")
    private Long subscribersCount = 0L;

    @Column(name = "subscribing_count", nullable = false, columnDefinition = "default 0")
    private Long subscribingCount = 0L;

    @Column(name = "views_count", nullable = false, columnDefinition = "default 0")
    private Long viewsCount = 0L;

    @Column(name = "likes_count", nullable = false, columnDefinition = "default 0")
    private Long likesCount = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SiteUser getOwner() {
        return owner;
    }

    public void setOwner(SiteUser owner) {
        this.owner = owner;
    }

    public Long getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(Long followersCount) {
        this.followersCount = followersCount;
    }

    public Long getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(Long followingCount) {
        this.followingCount = followingCount;
    }

    public Long getSubscribersCount() {
        return subscribersCount;
    }

    public void setSubscribersCount(Long subscribersCount) {
        this.subscribersCount = subscribersCount;
    }

    public Long getSubscribingCount() {
        return subscribingCount;
    }

    public void setSubscribingCount(Long subscribingCount) {
        this.subscribingCount = subscribingCount;
    }

    public Long getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(Long viewsCount) {
        this.viewsCount = viewsCount;
    }

    public Long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Long likesCount) {
        this.likesCount = likesCount;
    }

    @Override
    public String toString() {
        return "SiteUserInfo{" +
                "id=" + id +
                ", owner=" + owner +
                ", followersCount=" + followersCount +
                ", followingCount=" + followingCount +
                ", subscribersCount=" + subscribersCount +
                ", subscribingCount=" + subscribingCount +
                ", viewsCount=" + viewsCount +
                ", likesCount=" + likesCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SiteUserInfo that = (SiteUserInfo) o;

        if (!id.equals(that.id)) return false;
        if (!owner.equals(that.owner)) return false;
        if (!followersCount.equals(that.followersCount)) return false;
        if (!followingCount.equals(that.followingCount)) return false;
        if (!subscribersCount.equals(that.subscribersCount)) return false;
        if (!subscribingCount.equals(that.subscribingCount)) return false;
        if (!viewsCount.equals(that.viewsCount)) return false;
        return likesCount.equals(that.likesCount);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + owner.hashCode();
        result = 31 * result + followersCount.hashCode();
        result = 31 * result + followingCount.hashCode();
        result = 31 * result + subscribersCount.hashCode();
        result = 31 * result + subscribingCount.hashCode();
        result = 31 * result + viewsCount.hashCode();
        result = 31 * result + likesCount.hashCode();
        return result;
    }
}
