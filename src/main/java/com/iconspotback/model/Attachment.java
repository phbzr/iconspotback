package com.iconspotback.model;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;

@MappedSuperclass
public class Attachment {

    private static final int EXPIRATION = 60 * 12;

    @Column(name = "url", nullable = false)
    private String url;

    /**
     * Types of current attachments:
     * 1 - image
     * 2 - video
     * 4 - audio
     * 3 - file
     */
    @Column(name = "type", nullable = false)
    private Long type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_id", nullable = false)
    private SiteUser owner;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiry_date")
    private Date expiryDate;

    public Attachment() {
        super();
    }

    public Attachment(String url, Long type) {
        super();

        this.url = url;
        this.type = type;
        this.expiryDate = calculateExpiryDate(EXPIRATION);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public SiteUser getOwner() {
        return owner;
    }

    public void setOwner(SiteUser owner) {
        this.owner = owner;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    private Date calculateExpiryDate(final int expiryTimeInMinutes) {
        final Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(new Date().getTime());
        cal.add(Calendar.MINUTE, expiryTimeInMinutes);
        return new Date(cal.getTime().getTime());
    }

    @Override
    public String toString() {
        return "Attachment{" +
                "url='" + url + '\'' +
                ", type=" + type +
                ", owner=" + owner +
                ", expiryDate=" + expiryDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Attachment that = (Attachment) o;

        if (!url.equals(that.url)) return false;
        if (!type.equals(that.type)) return false;
        if (!owner.equals(that.owner)) return false;
        return expiryDate != null ? expiryDate.equals(that.expiryDate) : that.expiryDate == null;
    }

    @Override
    public int hashCode() {
        int result = url.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + owner.hashCode();
        result = 31 * result + (expiryDate != null ? expiryDate.hashCode() : 0);
        return result;
    }
}
