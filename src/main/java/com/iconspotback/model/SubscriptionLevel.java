package com.iconspotback.model;

import javax.persistence.*;

@Entity
@Table(name = "subscription_level")
public class SubscriptionLevel {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscription_level_generator")
    @SequenceGenerator(allocationSize = 1, name = "subscription_level_generator", sequenceName = "subscription_level_id_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "price", nullable = false)
    private Long price;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @OneToOne
    @JoinColumn(name = "subscription_level_settings", nullable = false)
    private SubscriptionLevelSettings  subscriptionLevelSettings;

    @Column(name = "small_icon")
    private String smallIcon;

    @Column(name = "large_icon")
    private String largeIcon;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SubscriptionLevelSettings getSubscriptionLevelSettings() {
        return subscriptionLevelSettings;
    }

    public void setSubscriptionLevelSettings(SubscriptionLevelSettings subscriptionLevelSettings) {
        this.subscriptionLevelSettings = subscriptionLevelSettings;
    }

    public String getSmallIcon() {
        return smallIcon;
    }

    public void setSmallIcon(String smallIcon) {
        this.smallIcon = smallIcon;
    }

    public String getLargeIcon() {
        return largeIcon;
    }

    public void setLargeIcon(String largeIcon) {
        this.largeIcon = largeIcon;
    }

    @Override
    public String toString() {
        return "SubscriptionLevel{" +
                "id=" + id +
                ", price=" + price +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", subscriptionLevelSettings=" + subscriptionLevelSettings +
                ", smallIcon='" + smallIcon + '\'' +
                ", largeIcon='" + largeIcon + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubscriptionLevel that = (SubscriptionLevel) o;

        if (!id.equals(that.id)) return false;
        if (!price.equals(that.price)) return false;
        if (!title.equals(that.title)) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (!subscriptionLevelSettings.equals(that.subscriptionLevelSettings)) return false;
        if (smallIcon != null ? !smallIcon.equals(that.smallIcon) : that.smallIcon != null) return false;
        return largeIcon != null ? largeIcon.equals(that.largeIcon) : that.largeIcon == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + price.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + subscriptionLevelSettings.hashCode();
        result = 31 * result + (smallIcon != null ? smallIcon.hashCode() : 0);
        result = 31 * result + (largeIcon != null ? largeIcon.hashCode() : 0);
        return result;
    }
}
