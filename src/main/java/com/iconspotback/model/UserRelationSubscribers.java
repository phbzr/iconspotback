package com.iconspotback.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "user_relation_subscribers")
public class UserRelationSubscribers {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_relation_subscribers_generator")
    @SequenceGenerator(allocationSize = 1, name = "user_relation_subscribers_generator", sequenceName = "user_relation_subscribers_id_seq")
    @Column(name = "id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "subscription_level", nullable = false)
    private SubscriptionLevel subscriptionLevel;

    @Column(name = "expiry_date", nullable = false)
    private Date expiryDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SubscriptionLevel getSubscriptionLevel() {
        return subscriptionLevel;
    }

    public void setSubscriptionLevel(SubscriptionLevel subscriptionLevel) {
        this.subscriptionLevel = subscriptionLevel;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public String toString() {
        return "UserRelationSubscribers{" +
                "id=" + id +
                ", subscriptionLevel=" + subscriptionLevel +
                ", expiryDate=" + expiryDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRelationSubscribers that = (UserRelationSubscribers) o;

        if (!id.equals(that.id)) return false;
        if (!subscriptionLevel.equals(that.subscriptionLevel)) return false;
        return expiryDate.equals(that.expiryDate);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + subscriptionLevel.hashCode();
        result = 31 * result + expiryDate.hashCode();
        return result;
    }
}
