package com.iconspotback.model;

import javax.persistence.*;

@Entity
@Table(name = "subscription_level_settings")
public class SubscriptionLevelSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscription_level_generator")
    @SequenceGenerator(allocationSize = 1, name = "subscription_level_generator", sequenceName = "subscription_level_id_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "can_private_message")
    private Boolean canPrivateMessage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getCanPrivateMessage() {
        return canPrivateMessage;
    }

    public void setCanPrivateMessage(Boolean canPrivateMessage) {
        this.canPrivateMessage = canPrivateMessage;
    }

    @Override
    public String toString() {
        return "SubscriptionLevelSettings{" +
                "id=" + id +
                ", canPrivateMessage=" + canPrivateMessage +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SubscriptionLevelSettings that = (SubscriptionLevelSettings) o;

        if (!id.equals(that.id)) return false;
        return canPrivateMessage != null ? canPrivateMessage.equals(that.canPrivateMessage) : that.canPrivateMessage == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (canPrivateMessage != null ? canPrivateMessage.hashCode() : 0);
        return result;
    }
}
