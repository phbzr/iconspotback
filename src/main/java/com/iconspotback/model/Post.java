package com.iconspotback.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "post")
public class Post extends TenantEntityWithCreationDate {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "post_generator")
    @SequenceGenerator(allocationSize = 1, name = "post_generator", sequenceName = "post_id_seq")
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "owner_id", nullable = false)
    private SiteUser owner;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "content")
    private String content;

    @Column(name = "likes_counter", nullable = false)
    private Long likesCounter;

    @Column(name = "views_counter", nullable = false)
    private Long viewsCounter;

    @Column(name = "access", nullable = false)
    private Integer access;

    @Column(name = "cover_image")
    private String coverImage;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "post_tags",
            joinColumns = {@JoinColumn(name = "POST_ID")},
            inverseJoinColumns = {@JoinColumn(name = "TAG_ID")})
    private Set<Tag> postTags = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "post_subscription_access",
            joinColumns = {@JoinColumn(name = "POST_ID")},
            inverseJoinColumns = {@JoinColumn(name = "SUBSCRIPTION_LEVEL_ID")})
    private Set<SubscriptionLevel> availableForSubscriptions = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SiteUser getOwner() {
        return owner;
    }

    public void setOwner(SiteUser owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getLikesCounter() {
        return likesCounter;
    }

    public void setLikesCounter(Long likesCounter) {
        this.likesCounter = likesCounter;
    }

    public Long getViewsCounter() {
        return viewsCounter;
    }

    public void setViewsCounter(Long viewsCounter) {
        this.viewsCounter = viewsCounter;
    }

    public Integer getAccess() {
        return access;
    }

    public void setAccess(Integer access) {
        this.access = access;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String postCover) {
        this.coverImage = postCover;
    }

    public Set<Tag> getPostTags() {
        return postTags;
    }

    public void setPostTags(Set<Tag> postTags) {
        this.postTags = postTags;
    }

    public Set<SubscriptionLevel> getAvailableForSubscriptions() {
        return availableForSubscriptions;
    }

    public void setAvailableForSubscriptions(Set<SubscriptionLevel> availableForSubscriptions) {
        this.availableForSubscriptions = availableForSubscriptions;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", owner=" + owner +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", likesCounter=" + likesCounter +
                ", viewsCounter=" + viewsCounter +
                ", access=" + access +
                ", coverImage='" + coverImage + '\'' +
                ", postTags='" + postTags + '\'' +
                ", availableForSubscriptions='" + availableForSubscriptions + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Post post = (Post) o;

        if (!id.equals(post.id)) return false;
        if (!owner.equals(post.owner)) return false;
        if (!title.equals(post.title)) return false;
        if (content != null ? !content.equals(post.content) : post.content != null) return false;
        if (!likesCounter.equals(post.likesCounter)) return false;
        if (!viewsCounter.equals(post.viewsCounter)) return false;
        if (postTags != null ? !postTags.equals(post.postTags) : post.postTags != null) return false;
        if (!access.equals(post.access)) return false;
        if (availableForSubscriptions != null ?
                !availableForSubscriptions.equals(
                        post.availableForSubscriptions) : post.availableForSubscriptions != null) return false;
        return coverImage != null ? coverImage.equals(post.coverImage) : post.coverImage == null;
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + owner.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + likesCounter.hashCode();
        result = 31 * result + viewsCounter.hashCode();
        result = 31 * result + (postTags != null ? postTags.hashCode() : 0);
        result = 31 * result + access.hashCode();
        result = 31 * result + (availableForSubscriptions != null ? availableForSubscriptions.hashCode() : 0);
        result = 31 * result + (coverImage != null ? coverImage.hashCode() : 0);
        return result;
    }
}
