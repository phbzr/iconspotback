package com.iconspotback.model;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class UserRelation {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "target_by", nullable = false)
    private SiteUser targetBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "target", nullable = false)
    private SiteUser target;

    public SiteUser getTargetBy() {
        return targetBy;
    }

    public void setTargetBy(SiteUser targetBy) {
        this.targetBy = targetBy;
    }

    public SiteUser getTarget() {
        return target;
    }

    public void setTarget(SiteUser target) {
        this.target = target;
    }

    @Override
    public String toString() {
        return "UserRelation{" +
                "targetBy=" + targetBy +
                ", target=" + target +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRelation that = (UserRelation) o;

        if (!targetBy.equals(that.targetBy)) return false;
        return target.equals(that.target);
    }

    @Override
    public int hashCode() {
        int result = targetBy.hashCode();
        result = 31 * result + target.hashCode();
        return result;
    }
}
