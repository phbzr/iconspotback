package com.iconspotback.model;

import javax.persistence.*;

@Entity
@Table(name = "user_relation_followers")
public class UserRelationFollowers {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_relation_followers_generator")
    @SequenceGenerator(allocationSize = 1, name = "user_relation_followers_generator", sequenceName = "user_relation_followers_id_seq")
    @Column(name = "id")
    private Long id;

    @Override
    public String toString() {
        return "UserRelationFollowers{" +
                "id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserRelationFollowers that = (UserRelationFollowers) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
