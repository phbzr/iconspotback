package com.iconspotback.model.dto;

import com.iconspotback.model.Category;
import com.iconspotback.model.Tag;
import com.iconspotback.model.TenantEntityWithCreationDate;
import org.springframework.data.domain.Page;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PostDto extends TenantEntityWithCreationDate {
    private Long id;
    private SiteUserDto owner;
    private String title;
    private String content;
    private Long likesCounter;
    private Long viewsCounter;
    private Integer access;
    private String coverImage;
    private Set<Tag> postTags = new HashSet<>();
    private Page<PostCommentDto> comments;
    private List<PostAttachmentDto> attachments;
    private Set<Long> availableForSubscriptions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SiteUserDto getOwner() {
        return owner;
    }

    public void setOwner(SiteUserDto owner) {
        this.owner = owner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getLikesCounter() {
        return likesCounter;
    }

    public void setLikesCounter(Long likesCounter) {
        this.likesCounter = likesCounter;
    }

    public Long getViewsCounter() {
        return viewsCounter;
    }

    public void setViewsCounter(Long viewsCounter) {
        this.viewsCounter = viewsCounter;
    }

    public Integer getAccess() {
        return access;
    }

    public void setAccess(Integer access) {
        this.access = access;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public Set<Tag> getPostTags() {
        return postTags;
    }

    public void setPostTags(Set<Tag> postTags) {
        this.postTags = postTags;
    }

    public Page<PostCommentDto> getComments() {
        return comments;
    }

    public void setComments(Page<PostCommentDto> comments) {
        this.comments = comments;
    }

    public List<PostAttachmentDto> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<PostAttachmentDto> attachments) {
        this.attachments = attachments;
    }

    public Set<Long> getAvailableForSubscriptions() {
        return availableForSubscriptions;
    }

    public void setAvailableForSubscriptions(Set<Long> availableForSubscriptions) {
        this.availableForSubscriptions = availableForSubscriptions;
    }

    @Override
    public String toString() {
        return "PostDto{" +
                "id=" + id +
                ", owner=" + owner +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", likesCounter=" + likesCounter +
                ", viewsCounter=" + viewsCounter +
                ", access=" + access +
                ", coverImage='" + coverImage + '\'' +
                ", postTags=" + postTags +
                ", comments=" + comments +
                ", attachments=" + attachments +
                ", availableForSubscriptions=" + availableForSubscriptions +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostDto postDto = (PostDto) o;

        if (id != null ? !id.equals(postDto.id) : postDto.id != null) return false;
        if (owner != null ? !owner.equals(postDto.owner) : postDto.owner != null) return false;
        if (title != null ? !title.equals(postDto.title) : postDto.title != null) return false;
        if (content != null ? !content.equals(postDto.content) : postDto.content != null) return false;
        if (likesCounter != null ? !likesCounter.equals(postDto.likesCounter) : postDto.likesCounter != null)
            return false;
        if (viewsCounter != null ? !viewsCounter.equals(postDto.viewsCounter) : postDto.viewsCounter != null)
            return false;
        if (access != null ? !access.equals(postDto.access) : postDto.access != null) return false;
        if (coverImage != null ? !coverImage.equals(postDto.coverImage) : postDto.coverImage != null) return false;
        if (postTags != null ? !postTags.equals(postDto.postTags) : postDto.postTags != null) return false;
        if (comments != null ? !comments.equals(postDto.comments) : postDto.comments != null) return false;
        if (attachments != null ? !attachments.equals(postDto.attachments) : postDto.attachments != null) return false;
        return availableForSubscriptions != null ? availableForSubscriptions.equals(postDto.availableForSubscriptions) : postDto.availableForSubscriptions == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (likesCounter != null ? likesCounter.hashCode() : 0);
        result = 31 * result + (viewsCounter != null ? viewsCounter.hashCode() : 0);
        result = 31 * result + (access != null ? access.hashCode() : 0);
        result = 31 * result + (coverImage != null ? coverImage.hashCode() : 0);
        result = 31 * result + (postTags != null ? postTags.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (attachments != null ? attachments.hashCode() : 0);
        result = 31 * result + (availableForSubscriptions != null ? availableForSubscriptions.hashCode() : 0);
        return result;
    }
}
