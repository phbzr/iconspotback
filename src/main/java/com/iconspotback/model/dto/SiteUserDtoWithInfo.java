package com.iconspotback.model.dto;

import com.iconspotback.model.SiteUserInfo;

public class SiteUserDtoWithInfo extends SiteUserDto {

    private Long followersCount;
    private Long viewsCount;
    private Long likesCount;

    public SiteUserDtoWithInfo() {
    }

    public SiteUserDtoWithInfo(SiteUserDto siteUserDto, SiteUserInfo siteUserInfo) {
        super(siteUserDto);
        this.followersCount = siteUserInfo.getFollowersCount();
        this.viewsCount = siteUserInfo.getViewsCount();
        this.likesCount = siteUserInfo.getLikesCount();
    }

    public Long getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(Long followersCount) {
        this.followersCount = followersCount;
    }

    public Long getViewsCount() {
        return viewsCount;
    }

    public void setViewsCount(Long viewsCount) {
        this.viewsCount = viewsCount;
    }

    public Long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Long likesCount) {
        this.likesCount = likesCount;
    }

    @Override
    public String toString() {
        return "SiteUserDtoWithInfo{" +
                "followersCount=" + followersCount +
                ", viewsCount=" + viewsCount +
                ", likesCount=" + likesCount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SiteUserDtoWithInfo that = (SiteUserDtoWithInfo) o;

        if (followersCount != null ? !followersCount.equals(that.followersCount) : that.followersCount != null)
            return false;
        if (viewsCount != null ? !viewsCount.equals(that.viewsCount) : that.viewsCount != null) return false;
        return likesCount != null ? likesCount.equals(that.likesCount) : that.likesCount == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (followersCount != null ? followersCount.hashCode() : 0);
        result = 31 * result + (viewsCount != null ? viewsCount.hashCode() : 0);
        result = 31 * result + (likesCount != null ? likesCount.hashCode() : 0);
        return result;
    }
}
