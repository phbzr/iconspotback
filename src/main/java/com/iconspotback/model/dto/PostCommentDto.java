package com.iconspotback.model.dto;

import com.iconspotback.model.TenantEntityWithCreationDate;

public class PostCommentDto extends TenantEntityWithCreationDate {
    private Long id;
    private SiteUserDto owner;
    private Long postId;
    private Long parentCommentId;
    private String content;
    private Long likesCounter;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SiteUserDto getOwner() {
        return owner;
    }

    public void setOwner(SiteUserDto owner) {
        this.owner = owner;
    }

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Long getParentCommentId() {
        return parentCommentId;
    }

    public void setParentCommentId(Long parentCommentId) {
        this.parentCommentId = parentCommentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getLikesCounter() {
        return likesCounter;
    }

    public void setLikesCounter(Long likesCounter) {
        this.likesCounter = likesCounter;
    }

    @Override
    public String toString() {
        return "CommentDto{" +
                "id=" + id +
                ", owner=" + owner +
                ", postId=" + postId +
                ", parentCommentId=" + parentCommentId +
                ", content='" + content + '\'' +
                ", likesCounter=" + likesCounter +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PostCommentDto that = (PostCommentDto) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (owner != null ? !owner.equals(that.owner) : that.owner != null) return false;
        if (postId != null ? !postId.equals(that.postId) : that.postId != null) return false;
        if (parentCommentId != null ? !parentCommentId.equals(that.parentCommentId) : that.parentCommentId != null)
            return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        return likesCounter != null ? likesCounter.equals(that.likesCounter) : that.likesCounter == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (postId != null ? postId.hashCode() : 0);
        result = 31 * result + (parentCommentId != null ? parentCommentId.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (likesCounter != null ? likesCounter.hashCode() : 0);
        return result;
    }
}
