package com.iconspotback.model.dto;

import com.iconspotback.model.UserProfile;

import java.util.Set;

public class SiteUserDto {

    private Long id;
    private String ssoId;
    private String name;
    private String avatarPath;
    private String profilePhoto;
    private Set<UserProfile> userProfiles;

    public SiteUserDto() {
    }

    public SiteUserDto(SiteUserDto siteUserDto) {
        this.id = siteUserDto.getId();
        this.ssoId = siteUserDto.getSsoId();
        this.name = siteUserDto.getName();
        this.avatarPath = siteUserDto.getAvatarPath();
        this.profilePhoto = siteUserDto.getProfilePhoto();
        this.userProfiles = siteUserDto.getUserProfiles();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSsoId() {
        return ssoId;
    }

    public void setSsoId(String ssoId) {
        this.ssoId = ssoId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public Set<UserProfile> getUserProfiles() {
        return userProfiles;
    }

    public void setUserProfiles(Set<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
    }

    @Override
    public String toString() {
        return "SiteUserDto{" +
                "id=" + id +
                ", ssoId='" + ssoId + '\'' +
                ", name='" + name + '\'' +
                ", avatarPath='" + avatarPath + '\'' +
                ", profilePhoto='" + profilePhoto + '\'' +
                ", userProfiles=" + userProfiles +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SiteUserDto that = (SiteUserDto) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (ssoId != null ? !ssoId.equals(that.ssoId) : that.ssoId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (avatarPath != null ? !avatarPath.equals(that.avatarPath) : that.avatarPath != null) return false;
        if (profilePhoto != null ? !profilePhoto.equals(that.profilePhoto) : that.profilePhoto != null) return false;
        return userProfiles != null ? userProfiles.equals(that.userProfiles) : that.userProfiles == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (ssoId != null ? ssoId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (avatarPath != null ? avatarPath.hashCode() : 0);
        result = 31 * result + (profilePhoto != null ? profilePhoto.hashCode() : 0);
        result = 31 * result + (userProfiles != null ? userProfiles.hashCode() : 0);
        return result;
    }
}
