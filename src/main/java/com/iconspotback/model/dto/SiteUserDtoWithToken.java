package com.iconspotback.model.dto;


import com.iconspotback.model.AuthToken;

public class SiteUserDtoWithToken extends SiteUserDto {
    private AuthToken authToken;

    public SiteUserDtoWithToken() {
    }

    public SiteUserDtoWithToken(SiteUserDto siteUserDto, AuthToken authToken) {
        super(siteUserDto);
        this.authToken = authToken;
    }

    public AuthToken getAuthToken() {
        return authToken;
    }

    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
    }
}
