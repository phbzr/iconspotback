package com.iconspotback.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Comment extends TenantEntityWithCreationDate {

    @ManyToOne
    @JoinColumn(name = "owner_id", nullable = false)
    private SiteUser owner;

    @Column(name = "content")
    private String content;

    @Column(name = "likes_counter")
    private Long likesCounter;

    public SiteUser getOwner() {
        return owner;
    }

    public void setOwner(SiteUser owner) {
        this.owner = owner;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getLikesCounter() {
        return likesCounter;
    }

    public void setLikesCounter(Long likesCounter) {
        this.likesCounter = likesCounter;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "owner=" + owner +
                ", content='" + content + '\'' +
                ", likesCounter=" + likesCounter +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Comment comment = (Comment) o;

        if (!owner.equals(comment.owner)) return false;
        if (content != null ? !content.equals(comment.content) : comment.content != null) return false;
        return likesCounter != null ? likesCounter.equals(comment.likesCounter) : comment.likesCounter == null;
    }

    @Override
    public int hashCode() {
        int result = owner.hashCode();
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (likesCounter != null ? likesCounter.hashCode() : 0);
        return result;
    }
}
