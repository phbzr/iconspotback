package com.iconspotback.dao;

import com.iconspotback.model.SiteUserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface SiteUserInfoDao extends JpaRepository<SiteUserInfo, Long> {

    @Transactional(readOnly = true)
    SiteUserInfo getByOwnerId(Long ownerId);
}
