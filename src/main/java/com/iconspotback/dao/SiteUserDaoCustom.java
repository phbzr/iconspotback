package com.iconspotback.dao;

import com.iconspotback.model.SiteUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SiteUserDaoCustom {

    Page<SiteUser> searchUsers(String value, Pageable pageable, Long authorizedUserId);
}
