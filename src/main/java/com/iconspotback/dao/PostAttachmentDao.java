package com.iconspotback.dao;

import com.iconspotback.model.PostAttachment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PostAttachmentDao extends JpaRepository<PostAttachment, Long> {

    @Transactional(readOnly = true)
    @Query("select p from PostAttachment p where p.post.id = :postId")
    List<PostAttachment> getPostAttachmentsByPostId(@Param("postId") Long postId);

}
