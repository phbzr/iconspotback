package com.iconspotback.dao;

import com.iconspotback.model.SiteUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface SiteUserDao extends JpaRepository<SiteUser, Long>, SiteUserDaoCustom {

    @Transactional(readOnly = true)
    SiteUser findBySsoId(String ssoId);

    @Transactional(readOnly = true)
    SiteUser findByEmail(String email);

}
