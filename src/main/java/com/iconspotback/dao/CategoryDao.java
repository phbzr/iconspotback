package com.iconspotback.dao;

import com.iconspotback.model.PostAttachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryDao extends JpaRepository<PostAttachment, Long> {
}
