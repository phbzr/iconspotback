package com.iconspotback.dao;

import com.iconspotback.model.UserProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface UserProfileDao extends JpaRepository<UserProfile, Long> {

    @Transactional(readOnly = true)
    UserProfile findByType(String type);
}
