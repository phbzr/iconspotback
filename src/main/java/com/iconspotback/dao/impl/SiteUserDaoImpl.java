package com.iconspotback.dao.impl;

import com.iconspotback.dao.SiteUserDaoCustom;
import com.iconspotback.model.SiteUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class SiteUserDaoImpl implements SiteUserDaoCustom {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public Page<SiteUser> searchUsers(String value, Pageable pageable, Long authorizedUserId) {
        Page<SiteUser> siteUsers;
        CriteriaBuilder cb1 = entityManager.getCriteriaBuilder();
        CriteriaQuery<SiteUser> criteriaQuery1 = cb1.createQuery(SiteUser.class);
        Root<SiteUser> userRoot = criteriaQuery1.from(SiteUser.class);
        EntityType<SiteUser> type = entityManager.getMetamodel().entity(SiteUser.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(cb1.notEqual(userRoot.get("id"), authorizedUserId));
        if (!value.equals("")) {
            predicates.add(cb1.or(
                    cb1.like(
                            cb1.lower(userRoot.get(type.getDeclaredSingularAttribute("ssoId", String.class))), value.toLowerCase() + "%"),
                    cb1.like(
                            cb1.lower(userRoot.get(type.getDeclaredSingularAttribute("name", String.class))), value.toLowerCase() + "%")));
        }
        criteriaQuery1.where(predicates.toArray(new Predicate[0]));

        TypedQuery<SiteUser> query1 = entityManager.createQuery(criteriaQuery1).
                setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
                .setMaxResults(pageable.getPageSize());
        List<SiteUser> resultList = query1.getResultList();

        CriteriaBuilder criteriaBuilder2 = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery2 = criteriaBuilder2.createQuery(Long.class);
        Root<SiteUser> root2 = criteriaQuery2.from(SiteUser.class);
        criteriaQuery2.select(criteriaBuilder2.count(root2)).where(predicates.toArray(new Predicate[0]));
        TypedQuery<Long> query2 = entityManager.createQuery(criteriaQuery2);
        Long totalCount = query2.getSingleResult();

        siteUsers = new PageImpl<>(resultList, pageable, totalCount);
        return siteUsers;
    }
}
