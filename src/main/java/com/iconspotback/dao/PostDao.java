package com.iconspotback.dao;

import com.iconspotback.model.Post;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface PostDao extends JpaRepository<Post, Long> {

    @Transactional(readOnly = true)
    @Query("select p from Post p where p.owner.ssoId = :userId order by p.creationDate asc")
    Page<Post> findByUser(@Param("userId") String userId, Pageable pageable);
}
