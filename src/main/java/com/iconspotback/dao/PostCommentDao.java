package com.iconspotback.dao;

import com.iconspotback.model.PostComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface PostCommentDao extends JpaRepository<PostComment, Long> {

    @Transactional(readOnly = true)
    @Query("select p from PostComment p where p.post.id = :postId order by p.creationDate asc")
    Page<PostComment> findByPost(@Param("postId") Long postId, Pageable pageable);
}
