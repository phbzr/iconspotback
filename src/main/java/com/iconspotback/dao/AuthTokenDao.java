package com.iconspotback.dao;

import com.iconspotback.model.AuthToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface AuthTokenDao extends JpaRepository<AuthToken, Long> {

    void deleteBySsoId(String ssoId);

    void deleteByJwtToken(String jwtToken);

    @Transactional(readOnly = true)
    AuthToken getAuthTokenByJwtTokenAndRefreshToken(String jwtToken, String refreshToken);
}
