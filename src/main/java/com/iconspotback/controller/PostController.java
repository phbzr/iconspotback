package com.iconspotback.controller;

import com.iconspotback.model.Post;
import com.iconspotback.model.SiteUser;
import com.iconspotback.model.dto.PostDto;
import com.iconspotback.service.AuthService;
import com.iconspotback.service.PostService;
import com.iconspotback.service.SiteUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RequestMapping("/posts")
@RestController
public class PostController {

    private PostService postService;
    private AuthService authService;
    private SiteUserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> addPost(@RequestBody PostDto postDto) {
        postService.addPost(postDto);
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = {"/{postId}"})
    public ResponseEntity<Void> deletePost(@PathVariable String postId, Principal principal) {
        Post post = postService.findById(Long.parseLong(postId));
        boolean isAdmin = authService.hasRole("ROLE_ADMIN");
        if (post.getOwner().getSsoId().equals(principal.getName()) || isAdmin) {
            postService.deletePost(Long.parseLong(postId));
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
        }
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Void> editPost(@RequestBody PostDto postDto, Principal principal) {
        boolean isAdmin = authService.hasRole("ROLE_ADMIN");
        if (postDto.getOwner().getSsoId().equals(principal.getName()) || isAdmin) {
            postService.editPost(postDto);
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
        }
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/{postId}"})
    public ResponseEntity<PostDto> getPost(@PathVariable String postId,
                                           @RequestParam(value = "needComments", required = false, defaultValue = "false") boolean needComments,
                                           @RequestParam(value = "needAttachments", required = false, defaultValue = "false") boolean needAttachments,
                                           Principal principal) {
        if (principal == null) {
            PostDto post = postService.findDtoById(Long.parseLong(postId), needComments, needAttachments);
            if (post.getAccess() == 1) {
                return new ResponseEntity<>(post, HttpStatus.OK);
            }
        } else {
//            SiteUser authorizedUser = userService.findBySsoId(principal.getName());
            PostDto post = postService.findDtoById(Long.parseLong(postId), needComments, needAttachments);
//            Integer accessLevel = postAccessService.checkAccess(post.getOwner().getId(), authorizedUser.getId());
//            if (post.getAccessType() <= Math.abs(accessLevel)) {
            return new ResponseEntity<>(post, HttpStatus.OK);
//            }
        }
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/users/{ssoId}"})
    public ResponseEntity<Page<PostDto>> getUserPosts(@PathVariable String ssoId,
                                                      @RequestParam(value = "page", required = false, defaultValue = "0") String page,
                                                      Principal principal) {
        SiteUser authorizedUser = userService.findBySsoId(principal.getName());
//        Integer accessLevel = postAccessService.checkAccess(userId, authorizedUser.getId());
        Integer access = 1;
        Page<PostDto> postPage = postService.findUserPosts(ssoId, access, PageRequest.of(Integer.parseInt(page), 20));
        return new ResponseEntity<>(postPage, HttpStatus.OK);
    }

    @Autowired
    public void setPostService(PostService postService) {
        this.postService = postService;
    }

    @Autowired
    public void setUserService(SiteUserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }
}
