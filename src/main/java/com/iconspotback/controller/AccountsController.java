package com.iconspotback.controller;

import com.iconspotback.config.JwtService;
import com.iconspotback.model.AuthToken;
import com.iconspotback.model.JwtRequest;
import com.iconspotback.model.SiteUser;
import com.iconspotback.model.dto.SiteUserDto;
import com.iconspotback.model.dto.SiteUserDtoWithToken;
import com.iconspotback.service.AuthService;
import com.iconspotback.service.AuthTokenService;
import com.iconspotback.service.DtoConverter;
import com.iconspotback.service.SiteUserService;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.UUID;

@RequestMapping("/accounts")
@RestController
public class AccountsController {

    private SiteUserService siteUserService;
    private AuthService authService;
    private AuthTokenService authTokenService;
    private DtoConverter dtoConverter;
    private JwtService jwtService;
    private PasswordEncoder passwordEncoder;

    @Autowired
    @Qualifier("customUserDetailsService")
    private UserDetailsService userDetailsService;

    /**
     * This method handles login GET requests.
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<SiteUserDto> login(@RequestBody JwtRequest authenticationRequest,
                                             @RequestParam(required = false) String deviceId) throws Exception {
        if (authenticationRequest.getUsername() == null || authenticationRequest.getPassword() == null) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }

        String lowerCaseUserName = authenticationRequest.getUsername().toLowerCase();
        SiteUser siteUser = siteUserService.findBySsoId(lowerCaseUserName);

        if (lowerCaseUserName.contains("@") && siteUser == null) {
            siteUser = siteUserService.findByEmail(lowerCaseUserName);
        }
        if (siteUser == null) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.UNAUTHORIZED);
        }
        authService.authenticate(siteUser.getSsoId(), authenticationRequest.getPassword());

        AuthToken authToken = authTokenService.creatAuthTokens(lowerCaseUserName);

        SiteUserDto authUser = dtoConverter.convertSiteUserToDto(siteUser);
        SiteUserDtoWithToken siteUserDtoWithToken = new SiteUserDtoWithToken(authUser, authToken);
        return new ResponseEntity<>(siteUserDtoWithToken, HttpStatus.OK);
    }

    /**
     * This methods refresh JWT by refresh token and expired jwt
     */
    @RequestMapping(value = "/token", method = RequestMethod.POST)
    public ResponseEntity<AuthToken> refreshJWT(@RequestBody AuthToken authToken) {

        if (authTokenService.validateToken(authToken)) {
            authTokenService.delete(authToken.getJwtToken());
            final UserDetails userDetails = userDetailsService
                    .loadUserByUsername(authToken.getSsoId());
            final String token = jwtService.generateToken(userDetails);

            AuthToken newAuthToken = new AuthToken(token, authToken.getRefreshToken(),
                    authToken.getSsoId(), Long.parseLong(jwtService.JWT_TOKEN_VALIDITY) - 10);
            authTokenService.save(newAuthToken);
            return new ResponseEntity<>(newAuthToken, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.UNAUTHORIZED);
    }

    @RequestMapping(method = RequestMethod.POST, value = {"/login/social"})
    public ResponseEntity<SiteUserDto> authBySocial(@RequestBody SiteUser socialUser) throws Exception {
        SiteUser currentUser = siteUserService.findBySsoId(socialUser.getSsoId());

        RandomString random = new RandomString();
        final String newPassword = random.nextString();
        final String hashedPassword = passwordEncoder.encode(newPassword);

        if (currentUser == null) {
            if (siteUserService.findByEmail(socialUser.getEmail()) != null) {
                return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CONFLICT);
            }
            currentUser = siteUserService.registerNewSocialUser(socialUser);
        }

        final String oldPassword = currentUser.getPassword();
        currentUser.setPassword(hashedPassword);
        siteUserService.editUser(currentUser);
        Authentication authentication = authService.authenticate(currentUser.getEmail(), newPassword);

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(socialUser.getSsoId());
        currentUser.setPassword(oldPassword);

        siteUserService.editUser(currentUser);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        final String token = jwtService.generateToken(userDetails);
        final String refreshToken = passwordEncoder.encode(UUID.randomUUID().toString());

        AuthToken authToken = new AuthToken(token, refreshToken, socialUser.getSsoId(),
                Long.parseLong(jwtService.JWT_TOKEN_VALIDITY) - 10);
        authTokenService.save(authToken);

        SiteUserDto authUser = dtoConverter.convertSiteUserToDto(currentUser);
        SiteUserDtoWithToken siteUserDtoWithToken = new SiteUserDtoWithToken(authUser, authToken);
        return new ResponseEntity<>(siteUserDtoWithToken, HttpStatus.OK);
    }

    /**
     * This method handles logout requests.
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public ResponseEntity<Void> logout(HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            SecurityContextHolder.getContext().setAuthentication(null);
            final String requestTokenHeader = request.getHeader("Authorization");
            if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
                String jwtToken = requestTokenHeader.substring(7);
                authTokenService.delete(jwtToken);
            }
        }
        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * This method will be called on form submission, handling POST request for
     * saving user in database.
     */
    @RequestMapping(value = {"/registration"}, method = RequestMethod.POST)
    public ResponseEntity<SiteUserDtoWithToken> registerUser(@RequestBody @Valid SiteUser siteUser) throws Exception {

        String password = siteUser.getPassword();
        if (!siteUserService.isUserSSOUnique(siteUser.getId(), siteUser.getSsoId())) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CONFLICT);
        } else if (siteUserService.findByEmail(siteUser.getEmail()) != null) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CONFLICT);
        }
        if (siteUser.getSsoId().equals("")) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CONFLICT);
        }
        if (siteUser.getEmail().equals("")) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CONFLICT);
        }
        if (siteUser.getPassword().equals("")) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.CONFLICT);
        }

        siteUserService.saveUser(siteUser);

        authService.authenticate(siteUser.getSsoId(), password);

        AuthToken authToken = authTokenService.creatAuthTokens(siteUser.getSsoId().toLowerCase());

        SiteUserDto authUser = dtoConverter.convertSiteUserToDto(siteUser);
        SiteUserDtoWithToken siteUserDtoWithToken = new SiteUserDtoWithToken(authUser, authToken);

        return new ResponseEntity<>(siteUserDtoWithToken, HttpStatus.OK);
    }

    @Autowired
    public void setSiteUserService(SiteUserService siteUserService) {
        this.siteUserService = siteUserService;
    }

    @Autowired
    public void setAuthService(AuthService authService) {
        this.authService = authService;
    }

    @Autowired
    public void setDtoConverter(DtoConverter dtoConverter) {
        this.dtoConverter = dtoConverter;
    }

    @Autowired
    public void setAuthTokenService(AuthTokenService authTokenService) {
        this.authTokenService = authTokenService;
    }

    @Autowired
    public void setJwtService(JwtService jwtService) {
        this.jwtService = jwtService;
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
}
