package com.iconspotback.controller;

import com.iconspotback.model.dto.SiteUserDto;
import com.iconspotback.model.dto.SiteUserDtoWithInfo;
import com.iconspotback.service.SiteUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/users")
@RestController
public class UsersController {

    @Value("${limit.per.users.page}")
    private int PAGE_SIZE;
    private SiteUserService userService;


    @RequestMapping(method = RequestMethod.GET, value = "/search")
    public ResponseEntity<Page<SiteUserDto>> searchUsers(@RequestParam(value = "value") String value,
                                                         @RequestParam(value = "page", required = false, defaultValue = "0") String page) {
        Page<SiteUserDto> requestedUsers = userService.searchUsers(value, PageRequest.of(Integer.parseInt(page), PAGE_SIZE));
        return new ResponseEntity<>(requestedUsers, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{ssoId}")
    public ResponseEntity<SiteUserDto> getUser(@PathVariable String ssoId) {
        SiteUserDtoWithInfo requestedUser = userService.getUserWithInfoBySsoId(ssoId);
        return new ResponseEntity<>(requestedUser, HttpStatus.OK);
    }

    @Autowired
    public void setUserService(SiteUserService userService) {
        this.userService = userService;
    }
}
